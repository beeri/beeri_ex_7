#include "Hexagon.h"

//print the info of the hexagon
void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "Area is " << CalArea() << std::endl;
}

//cal the area of the hexagon
double Hexagon::CalArea()
{
	return  mathUtils::hexagonArea(a);
}

//setter
void Hexagon::setA(double a)
{
	//if (std::cin.fail())
	//{
		//throw inputException();
	//}
	this->a = a;
}