#include "MathUtils.h"
#include <math.h>


// Function to find area of pentagon 
double mathUtils::pentagonArea(double a)
{
	double area;

	// Formula to find area 
	area = (sqrt(5 * (5 + 2 * (sqrt(5)))) * a * a) / 4;

	return area;
}

// function for calculating
// area of the hexagon. 
double mathUtils::hexagonArea(double a)
{
	double area;

	area = ((3 * sqrt(3) * (a * a)) / 2);

	// Formula to find area 
	return area;
		
}
