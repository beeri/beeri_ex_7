#pragma once
#include "shape.h"
#include "MathUtils.h"

class Pentagon : public Shape //, public mathUtils
{
public:
	Pentagon(std::string nam, std::string col, double a)
		:Shape(col, nam) {
		//void setName(string nam);
		//void setColor(string col); <-this redefines it d/n use
		setA(a);
	}
	virtual void draw();
	virtual double CalArea();
	void setA(double a);

private:
	double a;
};

